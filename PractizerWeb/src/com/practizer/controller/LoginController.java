package com.practizer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author kos
 *
 */

@Controller
public class LoginController {

	@RequestMapping(value="/login")
	String loginSuccess() {
		return "Login success";
	}
}
