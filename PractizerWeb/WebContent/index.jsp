<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Practizer Web</title>
<meta name="description" content="Practizer">
<meta name="author" content="dkos">

<base href="${pageContext.request.contextPath}">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="${resources}img/icons/favicon.ico"
	type="image/x-icon">

<link rel="apple-touch-icon" href="img/icons/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72"
	href="img/icons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114"
	href="img/icons/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link href="<c:url value="/resources/css/bootstrap.css"/>"
	rel="stylesheet">
<link
	href="<c:url value="/resources/fonts/font-awesome/css/font-awesome.css"/>"
	rel="stylesheet">

<!-- Stylesheet
    ================================================== -->
<!-- Modal Window -->
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700'
	rel='stylesheet' type='text/css'>
<!--  spring JSTL for resources: -->
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="<c:url value="/resources/css/modal/reset.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/modal/modal.css" />"
	rel="stylesheet">
<link
	href="<c:url value="/resources/css/nivo-lightbox/nivo-lightbox.css" />"
	rel="stylesheet">
<link href="<c:url value="/resources/css/nivo-lightbox/default.css" />"
	rel="stylesheet">
<link
	href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800,600,300'
	rel='stylesheet' type='text/css'>


<!-- modernizr.custom NOT working with modernizr-custom-svg.js file !!!!!!!!!!!!! -->
<!--<script type="text/javascript" src="js/modernizr-custom-svg.js"></script>-->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
	<!-- Navigation
    ==========================================-->
	<nav id="menu" class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand page-scroll" href="#page-top"><i
				class="fa fa-play fa-rotate-270"></i> Practizer</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right js-signin-modal-trigger">
				<li><a href="#page-top" class="page-scroll">Home</a></li>
				<li><a class="page-scroll" href="#0" data-signin="login">Login</a></li>
				<li><a href="#about" class="page-scroll">About</a></li>
				<li><a href="#portfolio" class="page-scroll">Preview</a></li>
				<li><a href="#contact" class="page-scroll">Contact</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid --> </nav>
	<!-- Modal Window -->
	<div class="cd-signin-modal js-signin-modal">
		<!-- this is the entire modal form, including the background -->
		<div class="cd-signin-modal__container">
			<!-- this is the container wrapper -->
			<ul
				class="cd-signin-modal__switcher js-signin-modal-switcher js-signin-modal-trigger">
				<li><a href="#0" data-signin="login" data-type="login">Sign
						in</a></li>
				<li><a href="#0" data-signin="signup" data-type="signup">New
						account</a></li>
			</ul>

			<div class="cd-signin-modal__block js-signin-modal-block"
				data-type="login">
				<!-- log in form -->
				<form class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
							for="signin-email">E-mail</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-email" type="email" placeholder="E-mail"> <span
							class="cd-signin-modal__error">Error message here!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
							for="signin-password">Password</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signin-password" type="text" placeholder="Password">
						<a href="#0"
							class="cd-signin-modal__hide-password js-hide-password">Hide</a>
						<span class="cd-signin-modal__error">Error message here!</span>
					</p>

					<!--<p class="cd-signin-modal__fieldset">
						<input type="checkbox" id="remember-me" checked class="cd-signin-modal__input ">
						<label for="remember-me">Remember me</label>
					</p>-->

					<p class="cd-signin-modal__fieldset">
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width"
							type="submit" value="Login">
					</p>
				</form>

				<p class="cd-signin-modal__bottom-message js-signin-modal-trigger">
					<a href="#0" data-signin="reset">Forgot your password?</a>
				</p>
			</div>
			<!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block"
				data-type="signup">
				<!-- sign up form -->
				<form class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--username cd-signin-modal__label--image-replace"
							for="signup-username">Username</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signup-username" type="text" placeholder="Username">
						<span class="cd-signin-modal__error">Error message here!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
							for="signup-email">E-mail</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signup-email" type="email" placeholder="E-mail"> <span
							class="cd-signin-modal__error">Error message here!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--password cd-signin-modal__label--image-replace"
							for="signup-password">Password</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="signup-password" type="text" placeholder="Password">
						<a href="#0"
							class="cd-signin-modal__hide-password js-hide-password">Hide</a>
						<span class="cd-signin-modal__error">Error message here!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<input type="checkbox" id="accept-terms"
							class="cd-signin-modal__input "> <label
							for="accept-terms">I agree to the <a href="#0">Terms</a></label>
					</p>

					<p class="cd-signin-modal__fieldset">
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
							type="submit" value="Create account">
					</p>
				</form>
			</div>
			<!-- cd-signin-modal__block -->

			<div class="cd-signin-modal__block js-signin-modal-block"
				data-type="reset">
				<!-- reset password form -->
				<p class="cd-signin-modal__message">Lost your password? Please
					enter your email address. You will receive a link to create a new
					password.</p>

				<form class="cd-signin-modal__form">
					<p class="cd-signin-modal__fieldset">
						<label
							class="cd-signin-modal__label cd-signin-modal__label--email cd-signin-modal__label--image-replace"
							for="reset-email">E-mail</label> <input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding cd-signin-modal__input--has-border"
							id="reset-email" type="email" placeholder="E-mail"> <span
							class="cd-signin-modal__error">Error message here!</span>
					</p>

					<p class="cd-signin-modal__fieldset">
						<input
							class="cd-signin-modal__input cd-signin-modal__input--full-width cd-signin-modal__input--has-padding"
							type="submit" value="Reset password">
					</p>
				</form>

				<p class="cd-signin-modal__bottom-message js-signin-modal-trigger">
					<a href="#0" data-signin="login">Back to log-in</a>
				</p>
			</div>
			<!-- cd-signin-modal__block -->
			<a href="#0" class="cd-signin-modal__close js-close">Close</a>
		</div>
		<!-- cd-signin-modal__container -->
	</div>
	<!-- cd-signin-modal -->

	<!-- Header -->
	<header id="header">
	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="intro-text">
					<h1>Practizer</h1>
					<!-- <p>Instruments • Preview • Product Overview</p>-->
					<a class="btn btn-custom btn-lg js-signin-modal-trigger" href="#0"
						data-signin="login">Login</a>
				</div>
			</div>
		</div>
	</div>
	</header>
	<!-- About Section -->
	<div id="about">
		<div class="container">
			<div class="section-title text-center center">
				<h2>About</h2>
				<hr>
			</div>
			<div class="row">
				<div class="col-xs-12 col-md-6">
					<img class="img-responsive" src="<c:url value="/resources/img/about.jpg" />"  alt="PracizerAbout"/>
					
				</div>
				<div class="col-xs-12 col-md-6">
					<div class="about-text">
						<p>Web Application to practice Songs with different
							Instruments</p>
						<p>Full controll with statistics for your perfect learning
							curve</p>
						<p>Choose your prefered genres from a library of Songs</p>
						<p>Train your instrument skills with much more fun</p>
						<a href="#portfolio" class="btn btn-default btn-lg page-scroll">Buy
							Now</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Portfolio Section -->
	<div id="portfolio">
		<div class="container">
			<div class="section-title text-center center">
				<h2>Preview</h2>
				<hr>
			</div>
			<div class="categories">
				<ul class="cat">
					<li>
						<ol class="type">
							<li><a href="#" data-filter="*" class="active">All</a></li>
							<li><a href="#" data-filter=".songs">Practizer - Songs</a></li>
							<li><a href="#" data-filter=".statistics">Statistics</a></li>
							<li><a href="#" data-filter=".product">Product Design</a></li>
						</ol>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="row">
				<div class="portfolio-items">
					<div class="col-sm-6 col-md-3 col-lg-3 songs">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/01-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/01-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 product">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/02-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/02-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 songs">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/03-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/03-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 songs">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/04-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/04-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 product">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/05-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/05-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 statistics">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/06-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/06-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 statistics">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/07-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/07-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 songs">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/08-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/08-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 product">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/09-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/09-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 statistics">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/10-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img
									src="<c:url value="/resources/img/portfolio/10-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 statistics">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/11-large.jpg" />" title="PractizerPreview"
									title="Project Title" data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/11-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3 col-lg-3 songs">
						<div class="portfolio-item">
							<div class="hover-bg">
								<a href="<c:url value="/resources/img/portfolio/12-large.jpg" />" title="PractizerPreview"
									data-lightbox-gallery="gallery1">
									<div class="hover-text">
										<h4>Project Title</h4>
									</div> <img src="<c:url value="/resources/img/portfolio/12-small.jpg" />"
									alt="PractizerPreview" />
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Contact Section -->
	<div id="contact" class="text-center">
		<div class="container">
			<div class="section-title center">
				<h2>Get In Touch</h2>
				<hr>
			</div>
			<div class="col-md-8 col-md-offset-2">
				<form name="sentMessage" id="contactForm" novalidate>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<input type="text" id="name" class="form-control"
									placeholder="Name" required>
								<p class="help-block text-danger"></p>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<input type="email" id="email" class="form-control"
									placeholder="Email" required>
								<p class="help-block text-danger"></p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<textarea name="message" id="message" class="form-control"
							rows="4" placeholder="Message" required></textarea>
						<p class="help-block text-danger"></p>
					</div>
					<div id="success"></div>
					<button type="submit" class="btn btn-default btn-lg">Send
						Message</button>
				</form>
				<div class="social">
					<ul>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-soundcloud"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div class="container text-center">
			<div class="fnav">
				<p>
					Copyright &copy; 2018 Music Master. Designed by <a
						href="http://www.dkos.at" rel="nofollow">dkos</a>
				</p>
			</div>
		</div>
	</div>
	<!-- SVG using Modernizr -->
	<!--<img src="img/logo/logo.png">-->
	<!-- SVG image: -->
	<!--<svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="282.222mm" height="282.222mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd"
viewBox="0 0 28222 28222"
 xmlns:xlink="http://www.w3.org/1999/xlink">
 <defs>
  <style type="text/css">
   <![CDATA[
    .str0 {stroke:#2B2A29;stroke-width:17.64}
    .str1 {stroke:white;stroke-width:7.62}
    .fil2 {fill:none}
    .fil4 {fill:none;fill-rule:nonzero}
    .fil3 {fill:#2B2A29;fill-rule:nonzero}
    .fil0 {fill:#231F1F;fill-rule:nonzero}
    .fil1 {fill:#F0F2F2;fill-rule:nonzero}
   ]]>
  </style>
 </defs>
 <g id="Layer_x0020_1">
  <metadata id="CorelCorpID_0Corel-Layer"/>
  <g id="_380041718448">
   <polygon class="fil0" points="-25744,15009 -42374,15009 -42374,5770 -25744,5770 "/>
   <g>
    <path class="fil1" d="M-41675 14721c0,137 -111,248 -248,248l-454 0 0 -5711 454 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-40306 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-38937 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-37568 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-36199 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-34830 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-33461 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-32092 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-30723 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-29354 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-27985 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-26616 14721c0,137 -111,248 -248,248l-706 0c-136,0 -247,-111 -247,-248l0 -5215c0,-137 111,-248 247,-248l706 0c137,0 248,111 248,248l0 5215z"/>
    <path class="fil1" d="M-25747 14969l-454 0c-137,0 -248,-111 -248,-248l0 -5215c0,-137 111,-248 248,-248l454 0 0 5711z"/>
    <path class="fil0" d="M-41247 12392c0,78 -63,142 -142,142l-405 0c-78,0 -142,-64 -142,-142l0 -2992c0,-78 64,-142 142,-142l405 0c79,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-39892 12392c0,78 -64,142 -142,142l-405 0c-79,0 -142,-64 -142,-142l0 -2992c0,-78 63,-142 142,-142l405 0c78,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-37132 12392c0,78 -63,142 -142,142l-405 0c-78,0 -142,-64 -142,-142l0 -2992c0,-78 64,-142 142,-142l405 0c79,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-35763 12392c0,78 -63,142 -142,142l-405 0c-78,0 -142,-64 -142,-142l0 -2992c0,-78 64,-142 142,-142l405 0c79,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-33026 12392c0,78 -64,142 -142,142l-405 0c-78,0 -142,-64 -142,-142l0 -2992c0,-78 64,-142 142,-142l405 0c78,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-31657 12392c0,78 -64,142 -142,142l-405 0c-79,0 -142,-64 -142,-142l0 -2992c0,-78 63,-142 142,-142l405 0c78,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-30303 12392c0,78 -63,142 -142,142l-405 0c-78,0 -142,-64 -142,-142l0 -2992c0,-78 64,-142 142,-142l405 0c79,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-27542 12392c0,78 -64,142 -142,142l-405 0c-79,0 -142,-64 -142,-142l0 -2992c0,-78 63,-142 142,-142l405 0c78,0 142,64 142,142l0 2992z"/>
    <path class="fil0" d="M-26174 12392c0,78 -63,142 -142,142l-404 0c-79,0 -142,-64 -142,-142l0 -2992c0,-78 63,-142 142,-142l404 0c79,0 142,64 142,142l0 2992z"/>
   </g>
  </g>
  <rect class="fil2 str0" x="-34754" y="9186" width="5495" height="5834"/>
  <g id="_380034384832">
   <g>
    <polygon class="fil0" points="14226,16571 8767,16571 8767,10784 14226,10784 "/>
    <g>
     <path class="fil1" d="M10052 16284c0,136 -111,246 -246,246l-702 0c-136,0 -246,-110 -246,-246l0 -5182c0,-135 110,-246 246,-246l702 0c135,0 246,111 246,246l0 5182z"/>
     <path class="fil1" d="M11412 16284c0,136 -110,246 -246,246l-702 0c-135,0 -246,-110 -246,-246l0 -5182c0,-135 111,-246 246,-246l702 0c136,0 246,111 246,246l0 5182z"/>
     <path class="fil1" d="M12772 16284c0,136 -110,246 -246,246l-701 0c-136,0 -246,-110 -246,-246l0 -5182c0,-135 110,-246 246,-246l701 0c136,0 246,111 246,246l0 5182z"/>
     <path class="fil1" d="M14132 16284c0,136 -110,246 -246,246l-701 0c-136,0 -246,-110 -246,-246l0 -5182c0,-135 110,-246 246,-246l701 0c136,0 246,111 246,246l0 5182z"/>
     <path class="fil0" d="M10484 13970c0,78 -63,141 -141,141l-402 0c-78,0 -142,-63 -142,-141l0 -2973c0,-77 64,-141 142,-141l402 0c78,0 141,64 141,141l0 2973z"/>
     <path class="fil0" d="M11844 13970c0,78 -63,141 -141,141l-402 0c-78,0 -141,-63 -141,-141l0 -2973c0,-77 63,-141 141,-141l402 0c78,0 141,64 141,141l0 2973z"/>
     <path class="fil0" d="M13190 13970c0,78 -63,141 -141,141l-403 0c-78,0 -141,-63 -141,-141l0 -2973c0,-77 63,-141 141,-141l403 0c78,0 141,64 141,141l0 2973z"/>
    </g>
   </g>
   <path class="fil3" d="M11533 14177c-19,19 -30,39 -34,58 -4,-66 -43,-96 -116,-96l-617 0c-142,0 -216,77 -216,227l0 2167 224 0 0 -2171 609 0 0 2171 224 0 0 -2171 609 0 0 2171 220 0 0 -2167c0,-150 -70,-227 -212,-227l-617 0c-23,0 -47,11 -74,38z"/>
   <path id="1" class="fil4 str1" d="M11533 14177c-19,19 -30,39 -34,58 -4,-66 -43,-96 -116,-96l-617 0c-142,0 -216,77 -216,227l0 2167 224 0 0 -2171 609 0 0 2171 224 0 0 -2171 609 0 0 2171 220 0 0 -2167c0,-150 -70,-227 -212,-227l-617 0c-23,0 -47,11 -74,38z"/>
   <path class="fil3" d="M13083 15059l-234 0 0 1246c0,157 77,238 226,238l855 0c149,0 225,-81 225,-238l0 -1246 -234 0 0 1250 -838 0 0 -1250z"/>
   <path id="1" class="fil4 str1" d="M13083 15059l-234 0 0 1246c0,157 77,238 226,238l855 0c149,0 225,-81 225,-238l0 -1246 -234 0 0 1250 -838 0 0 -1250z"/>
   <path class="fil3" d="M14680 16161l-234 0 0 157c0,153 81,230 238,230l911 0c157,0 238,-77 238,-230l0 -395c0,-157 -77,-238 -234,-238l-919 0 0 -387 919 0 0 153 234 0 0 -149c0,-157 -77,-238 -226,-238l-935 0c-149,0 -226,81 -226,238l0 391c0,153 77,230 234,230l919 0 0 391 -919 0 0 -153z"/>
   <path id="1" class="fil3" d="M16389 16548l0 -1484 -234 0 0 1484 234 0zm0 -1721l0 -234 -234 0 0 234 234 0z"/>
   <path id="2" class="fil3" d="M16945 15298l919 0 0 234 234 0 0 -230c0,-157 -76,-238 -225,-238l-936 0c-149,0 -225,81 -225,238l0 1016c0,153 80,230 237,230l911 0c158,0 238,-77 238,-230l0 -238 -234 0 0 234 -919 0 0 -1016z"/>
   <path id="3" class="fil3" d="M19449 14085c-20,20 -33,40 -37,60 -4,-68 -44,-100 -121,-100l-645 0c-149,0 -225,80 -225,237l0 2266 234 0 0 -2270 636 0 0 2270 234 0 0 -2270 637 0 0 2270 230 0 0 -2266c0,-157 -73,-237 -222,-237l-645 0c-24,0 -48,12 -76,40z"/>
   <path id="4" class="fil3" d="M20948 15923l919 0 0 391 -919 0 0 -391zm1153 -629c0,-153 -81,-230 -238,-230l-911 0c-157,0 -238,77 -238,230l0 238 234 0 0 -234 919 0 0 387 -915 0c-157,0 -238,77 -238,234l0 391c0,157 77,238 226,238l1161 0 0 -1254z"/>
   <path id="5" class="fil3" d="M22657 16161l-233 0 0 157c0,153 80,230 237,230l911 0c158,0 238,-77 238,-230l0 -395c0,-157 -76,-238 -234,-238l-919 0 0 -387 919 0 0 153 234 0 0 -149c0,-157 -76,-238 -226,-238l-935 0c-149,0 -225,81 -225,238l0 391c0,153 76,230 233,230l919 0 0 391 -919 0 0 -153z"/>
   <polygon id="6" class="fil3" points="24242,14045 24242,15064 23971,15064 23971,15298 24242,15298 24242,16548 24483,16548 24483,15298 24753,15298 24753,15064 24483,15064 24483,14045 "/>
   <path id="7" class="fil3" d="M26068 15685l-920 0 0 -387 920 0 0 387zm-920 629l0 -391 916 0c157,0 237,-77 237,-230l0 -391c0,-157 -76,-238 -225,-238l-936 0c-149,0 -225,81 -225,238l0 1016c0,153 80,230 238,230l911 0c157,0 237,-77 237,-230l0 -238 -233 0 0 234 -920 0z"/>
   <path id="8" class="fil3" d="M26858 16548l0 -1250 516 0 0 234 233 0 0 -230c0,-157 -76,-238 -225,-238l-532 0c-150,0 -226,81 -226,238l0 1246 234 0z"/>
   <path id="9" class="fil4 str1" d="M14680 16161l-234 0 0 157c0,153 81,230 238,230l911 0c157,0 238,-77 238,-230l0 -395c0,-157 -77,-238 -234,-238l-919 0 0 -387 919 0 0 153 234 0 0 -149c0,-157 -77,-238 -226,-238l-935 0c-149,0 -226,81 -226,238l0 391c0,153 77,230 234,230l919 0 0 391 -919 0 0 -153z"/>
   <path id="10" class="fil4 str1" d="M16389 16548l0 -1484 -234 0 0 1484 234 0zm0 -1721l0 -234 -234 0 0 234 234 0z"/>
   <path id="11" class="fil4 str1" d="M16945 15298l919 0 0 234 234 0 0 -230c0,-157 -76,-238 -225,-238l-936 0c-149,0 -225,81 -225,238l0 1016c0,153 80,230 237,230l911 0c158,0 238,-77 238,-230l0 -238 -234 0 0 234 -919 0 0 -1016z"/>
   <path id="12" class="fil4 str1" d="M19449 14085c-20,20 -33,40 -37,60 -4,-68 -44,-100 -121,-100l-645 0c-149,0 -225,80 -225,237l0 2266 234 0 0 -2270 636 0 0 2270 234 0 0 -2270 637 0 0 2270 230 0 0 -2266c0,-157 -73,-237 -222,-237l-645 0c-24,0 -48,12 -76,40z"/>
   <path id="13" class="fil4 str1" d="M20948 15923l919 0 0 391 -919 0 0 -391zm1153 -629c0,-153 -81,-230 -238,-230l-911 0c-157,0 -238,77 -238,230l0 238 234 0 0 -234 919 0 0 387 -915 0c-157,0 -238,77 -238,234l0 391c0,157 77,238 226,238l1161 0 0 -1254z"/>
   <path id="14" class="fil4 str1" d="M22657 16161l-233 0 0 157c0,153 80,230 237,230l911 0c158,0 238,-77 238,-230l0 -395c0,-157 -76,-238 -234,-238l-919 0 0 -387 919 0 0 153 234 0 0 -149c0,-157 -76,-238 -226,-238l-935 0c-149,0 -225,81 -225,238l0 391c0,153 76,230 233,230l919 0 0 391 -919 0 0 -153z"/>
   <polygon id="15" class="fil4 str1" points="24242,14045 24242,15064 23971,15064 23971,15298 24242,15298 24242,16548 24483,16548 24483,15298 24753,15298 24753,15064 24483,15064 24483,14045 "/>
   <path id="16" class="fil4 str1" d="M26068 15685l-920 0 0 -387 920 0 0 387zm-920 629l0 -391 916 0c157,0 237,-77 237,-230l0 -391c0,-157 -76,-238 -225,-238l-936 0c-149,0 -225,81 -225,238l0 1016c0,153 80,230 238,230l911 0c157,0 237,-77 237,-230l0 -238 -233 0 0 234 -920 0z"/>
   <path id="17" class="fil4 str1" d="M26858 16548l0 -1250 516 0 0 234 233 0 0 -230c0,-157 -76,-238 -225,-238l-532 0c-150,0 -226,81 -226,238l0 1246 234 0z"/>
  </g>
 </g>
</svg>-->
	<!--// Check if browser can handle SVG -->
	<script>
		if (!Modernizr.svg) {
			// Get all img tag of the document and create variables
			var i = document.getElementsByTagName("img"),
				j,
				y;
			// For each img tag
			for (j = i.length; j--;) {
				y = i[j].src
				// If filenames ends with SVG
				if (y.match(/svg$/)) {
					// Replace "svg" by "png"
					i[j].src = y.slice(0, -3) + 'png'
				}
			}
		}
	</script>
	<!-- Resource JavaScript -->
	<script type="text/javascript"
		src="<c:url value="/resources/js/modernizr.custom.js" />"></script>
	<!-- <script src="js/placeholders.min.js"></script> Modal: polyfill for the HTML5 placeholder attribute -->
	<script type="text/javascript"
		src="<c:url value="/resources/js/placeholder.min.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/modal.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/jquery.1.11.1.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/bootstrap.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/SmoothScroll.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/nivo-lightbox.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/jquery.isotope.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/jqBootstrapValidation.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/contact_me.js" />"></script>
	<script type="text/javascript"
		src="<c:url value="/resources/js/main.js" />"></script>
</body>
</html>